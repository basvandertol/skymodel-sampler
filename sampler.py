#!/usr/bin/env python2

from casacore.quanta import quantity
import casacore.images
import numpy

from idgwindow2 import idgwindow


skymodel_filename = "/var/scratch/bvdtol/tests/skymodel_sampler/Kimage_full_ampphase1.app.restored.pybdsm.sky_in.grouped.adjusted"
modelimagename = "/var/scratch/bvdtol/tests/skymodel_sampler/model.img"

def GaussianCoherence(major, minor, orientation, u, v):

    phi = (270-orientation) * (numpy.pi/180.0)
    cosPhi = numpy.cos(phi)
    sinPhi = numpy.sin(phi)

    # Rotate (u, v) by the orientation and scale with the major and minor axis
    # lengths (FWHM). Take care of the conversion of FWHM to sigma.
    arcsec2rad = (numpy.pi / 3600.0) / 180.0;
    fwhm2sigma = 1.0 / (2.0 * numpy.sqrt(2.0 * numpy.log(2.0)));

    uPrime = major * (arcsec2rad * fwhm2sigma) * (u * cosPhi - v * sinPhi)

    vPrime = minor * (arcsec2rad * fwhm2sigma) * (u * sinPhi + v * cosPhi);

    # Compute uPrime^2 + vPrime^2 and pre-multiply with -2.0 * PI^2 / C^2.
    uvPrime = (-2.0 * numpy.pi * numpy.pi) * (numpy.square(uPrime) + numpy.square(vPrime))


    return numpy.exp(uvPrime);

source_types = set()

min_ra = float('inf')
max_ra = -float('inf')
min_dec = float('inf')
max_dec = -float('inf')

sources = []

with open(skymodel_filename) as skymodel:
    for i, line in enumerate(skymodel):

        # first line contains the FORMAT string
        # interpret format
        if i == 0:
            f = line.split("=",1)[1].strip()
            print(f)
            cols = [col.strip() for col in f.split(",")]
            print(cols)
            type_idx = cols.index("Type")
            ra_idx = cols.index("Ra")
            dec_idx = cols.index("Dec")
            major_axis_idx = cols.index("MajorAxis")
            minor_axis_idx = cols.index("MinorAxis")
            orientation_idx = cols.index("Orientation")
            I_idx = cols.index("I")
            continue

        # Skip lines starting with "#", they are comments
        if line[0] == "#": continue


        #if i > 2000 : break

        s = line.split(",")

        # Skip lines that contain no commas, they are probably just whitespace
        if len(s) == 1: continue


        source_type = s[type_idx].strip()

        # Skip lines that contain no Type, they are patch definitions
        if len(source_type) == 0: continue

        ra = quantity(s[ra_idx]).get_value("rad")
        dec = quantity(s[dec_idx]).get_value("rad")
        Stokes_I = s[I_idx]

        print(ra,dec)

        min_ra = min(min_ra, ra)
        max_ra = max(max_ra, ra)
        min_dec = min(min_dec, dec)
        max_dec = max(max_dec, dec)

        source = {"type": source_type, "ra": quantity(ra, "rad"), "dec": quantity(dec, "rad"), "I": float(Stokes_I)}

        if source_type == "GAUSSIAN":
            major_axis = s[major_axis_idx]
            minor_axis = s[minor_axis_idx]
            orientation = s[orientation_idx]
            source["major"] = float(major_axis)
            source["minor"] = float(minor_axis)
            source["orientation"] = float(orientation)
            print(major_axis, minor_axis, orientation)


        source_types.add(source_type)

        sources.append(source)

print(source_types)


ra = (min_ra+max_ra)/2
dec = (min_dec+max_dec)/2


imagesize = max((max_ra-min_ra)*numpy.cos(min_dec)*1.1, (max_dec-min_dec)*1.1)

resolution = quantity(5.0, "arcsec")

N = int(numpy.ceil(imagesize/resolution.get_value("rad")/2))*2


cs = casacore.images.coordinates.coordinatesystem({'direction0' : {}})

img = casacore.images.image("", shape = [N,N])
cs = img.coordinates()

unit = cs["direction"].get_unit()[0]

cs["direction"].set_referencepixel([N/2, N/2])
cs["direction"].set_referencevalue([quantity(dec, "rad").get_value(unit), quantity(ra, "rad").get_value(unit)])
cs["direction"].set_increment([resolution.get_value(unit), -resolution.get_value(unit)])

print(cs["direction"].get_referencepixel())
print(cs["direction"].get_referencevalue())
print(cs["direction"].get_increment())

img = casacore.images.image(modelimagename, shape = [N,N], coordsys=cs)

d = img.getdata()


subgridsize = 64

taper = idgwindow(subgridsize, 9, 1.25)
taper2 = taper[:, numpy.newaxis] * taper[numpy.newaxis, :]

x,y = numpy.meshgrid(numpy.arange(0.5, subgridsize, 1)/subgridsize-.5, numpy.arange(0.5, subgridsize, 1)/subgridsize-.5)

u = -y*100000
v = x*100000

for source in sources:
    pos = img.topixel([source["dec"].get_value(unit), source["ra"].get_value(unit)])
    rpos = numpy.round(pos)

    print(pos-rpos)

    subgrid = taper2 * numpy.exp(2*numpy.pi * (x*(pos[0]-rpos[0]) - y*(pos[1]-rpos[1])))

    if source["type"] == "GAUSSIAN":
        print "Gaussian"
        subgrid = subgrid*GaussianCoherence(source["major"], source["minor"], source["orientation"], u, v)


    subgrid = numpy.abs(numpy.fft.fftshift(numpy.fft.fft2(numpy.fft.fftshift(subgrid))))/(subgridsize*subgridsize)
    d[rpos[0]-subgridsize/2:rpos[0]+subgridsize/2, rpos[1]-subgridsize/2:rpos[1]+subgridsize/2] += source["I"]*subgrid

img.putdata(d)


