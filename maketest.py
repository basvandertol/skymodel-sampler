import os
from subprocess import call
import casacore.images
import numpy


# Return the fractional part of the floating point number x.
def fraction(x):
  return numpy.modf(x)[0]

# Convert an angle from degrees to radians.
def deg2rad(angle):
    return (angle * numpy.pi) / 180.0

# Convert an angle from radians to degrees.
def rad2deg(angle):
    return (angle * 180.0) / numpy.pi

# Compute hours, min, sec from an angle in radians.
def rad2ra(angle):
    deg = numpy.fmod(rad2deg(angle), 360.0)
    if deg < 0.0:
        deg += 360.0
    # Ensure positive output (deg could equal -0.0).
    deg = abs(deg)
    assert(deg < 360.0)

    return (int(deg / 15.0), int(fraction(deg / 15.0) * 60.0), fraction(deg * 4.0) * 60.0)

# Compute degrees, arcmin, arcsec from an angle in radians, in the range [-90.0, +90.0].
def rad2dec(angle):
    deg = numpy.fmod(rad2deg(angle), 360.0)
    if deg > 180.0:
        deg -= 360.0
    elif deg < -180.0:
        deg += 360.0

    sign = (deg < 0.0)
    deg = abs(deg)
    assert(deg <= 90.0)

    return (sign, int(deg), int(fraction(deg) * 60.0), fraction(deg * 60.0) * 60.0)

# Return string representation of the input right ascension (as returned by rad2ra).
def ra2str(ra):
    return "%02d:%02d:%07.4f" % ra

# Return string representation of the input declination (as returned by rad2dec).
def dec2str(dec):
    return "%s%02d.%02d.%07.4f" % ("-" if dec[0] else "+", dec[1], dec[2], dec[3])

cellsize = 2.0
imagesize = 18000
datadir = '/var/scratch/bvdtol/tests/data'
source_dir = os.path.dirname(os.path.realpath(__file__))

skymodel_file_name = 'test.skymodel'
sourcedb = 'test.sourcedb'


ms = os.path.join(datadir, "W0.ms")

#template = os.path.join(datadir,"template.fits")

#print "Creating template image from " + ms
#call(["wsclean", "-size", str(imagesize), str(imagesize), "-scale", "{0}asec".format(cellsize), "-interval", "0", "1", "-no-reorder", ms])
#call(["mv", "wsclean-image.fits", template])

#img = casacore.images.image(template)


#with open(skymodel_file_name, "w") as f_sky:
    #print >>f_sky, "FORMAT = Name, Type, Ra, Dec, I, Q, U, V, MajorAxis, MinorAxis, Orientation"

    ## single point source

    #x = 3120.2
    #y = 2150.7
    #c = img.toworld((0,0,x,y))
    #ra = c[3]
    #dec = c[2]
    ##print >>f_sky, "point-{0}-{1}, POINT, {2}, {3}, 1.0, 0, 0, 0, , , ".format(x, y, ra2str(rad2ra(ra)), dec2str(rad2dec(dec)))
    #print >>f_sky, "point-{0}-{1}, GAUSSIAN, {2}, {3}, 1.0, 0, 0, 0, 4,12 , 30".format(x, y, ra2str(rad2ra(ra)), dec2str(rad2dec(dec)))

    ## grid of point sources
    ##for x in numpy.linspace(50,151, 11):
        ##for y in numpy.linspace(50,151, 11):
            ##c = img.toworld((0,0,x,y))
            ##ra = c[3]
            ##dec = c[2]
            ##print >>f_sky, "point-{0}-{1}, POINT, {2}, {3}, 1.0, 0, 0, 0, , , ".format(x, y, ra2str(rad2ra(ra)), dec2str(rad2dec(dec)))

    ##for x in numpy.linspace(50,151, 11):
        ##y = 20
        ##c = img.toworld((0,0,x,y))
        ##ra = c[3]
        ##dec = c[2]
        ##print >>f_sky, "gaussian-{0}-{1}, GAUSSIAN, {2}, {3}, 12.0, 0, 0, 0, 60 , 180, {0}".format(x, y, ra2str(rad2ra(ra)), dec2str(rad2dec(dec)))


call(["makesourcedb", "in={0}".format(skymodel_file_name), 'format=Name, Type, Ra, Dec, I, Q, U, V, MajorAxis, MinorAxis, Orientation', "out={0}".format(sourcedb), "append=false"])

call(["DPPP",
        "msin="+ms,
        "msout=",
        "msout.datacolumn=DATA",
        "steps=[predict]",
        "predict.type=predict",
        "predict.sourcedb=" + sourcedb])

#call(["wsclean", "-size", str(imagesize), str(imagesize), "-scale", "{0}asec".format(cellsize), "-no-reorder", "-data-column", "DATA", ms])
