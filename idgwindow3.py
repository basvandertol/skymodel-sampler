import numpy

from matplotlib import pyplot as plt

def idgwindow(N=64):
    P = N/4

    M = 8*N

    oversampling = 20

    x = numpy.linspace(-M/2, M/2,M*oversampling, endpoint=False)+0.5

    X = x[:, numpy.newaxis] + numpy.arange(-N/2, N/2)[numpy.newaxis, :]

    Y1 = []
    Y2 = []
    Y3 = []

    shifts = numpy.linspace(-36.5, 36.5, 147)
    #shifts = [0]

    for s in shifts:
        Y = numpy.sinc(X) * numpy.exp(2*numpy.pi*1j*X*s/N)
        Y1 += [Y[:(M-N)/2*oversampling,:].copy()]
        Y2 += [Y[(M+N)/2*oversampling:,:].copy()]
        Y3 += [Y[(M-N+P)/2*oversampling:(M+N-P)/2*oversampling,:].copy()]

    A = numpy.concatenate(Y1+Y2+Y3)

    A = numpy.dot(A, numpy.concatenate((numpy.eye(N/2), numpy.eye(N/2)[:,::-1])))

    Ainv = numpy.linalg.pinv(A)

    a = numpy.sum(Ainv[:,-(N-P)*oversampling*len(shifts):], axis=1)

    return numpy.concatenate((a,a[::-1]))



#plt.semilogy(numpy.abs(numpy.dot(Y1, a)))
#plt.semilogy(numpy.abs(numpy.dot(Y2, a)))
#plt.semilogy(numpy.abs(numpy.dot(Y3, a)-1.0))

#plt.show()





