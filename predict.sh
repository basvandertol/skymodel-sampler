#wsclean -predict -name normalization -pol I -use-idg -idg-mode hybrid ../data/RX42_SB100-109.2ch10s.ms
#DPPP msin=../data/RX42_SB100-109.2ch10s.ms msin.datacolumn=MODEL_DATA msout=. msout.datacolumn=MODEL_NORMALIZATION steps=[]

wsclean -predict -pol IQUV -use-idg -idg-mode hybrid -no-reorder ../data/RX42_SB100-109.2ch10s.ms
