#!/usr/bin/env python2

from casacore.quanta import quantity
import casacore.images
import numpy
import os

from idgwindow3 import idgwindow


skymodel_filename = "test.skymodel"

def GaussianCoherence(major, minor, orientation, u, v):

    phi = (270-orientation) * (numpy.pi/180.0)
    cosPhi = numpy.cos(phi)
    sinPhi = numpy.sin(phi)

    # Rotate (u, v) by the orientation and scale with the major and minor axis
    # lengths (FWHM). Take care of the conversion of FWHM to sigma.
    arcsec2rad = (numpy.pi / 3600.0) / 180.0;
    fwhm2sigma = 1.0 / (2.0 * numpy.sqrt(2.0 * numpy.log(2.0)));

    uPrime = major * (arcsec2rad * fwhm2sigma) * (u * cosPhi - v * sinPhi)

    vPrime = minor * (arcsec2rad * fwhm2sigma) * (u * sinPhi + v * cosPhi);

    # Compute uPrime^2 + vPrime^2 and pre-multiply with -2.0 * PI^2 / C^2.
    uvPrime = (-2.0 * numpy.pi * numpy.pi) * (numpy.square(uPrime) + numpy.square(vPrime))


    return numpy.exp(uvPrime);

source_types = set()

min_ra = float('inf')
max_ra = -float('inf')
min_dec = float('inf')
max_dec = -float('inf')

sources = []

with open(skymodel_filename) as skymodel:
    for i, line in enumerate(skymodel):

        # first line contains the FORMAT string
        # interpret format
        if i == 0:
            f = line.split("=",1)[1].strip()
            print(f)
            cols = [col.strip() for col in f.split(",")]
            print(cols)
            type_idx = cols.index("Type")
            ra_idx = cols.index("Ra")
            dec_idx = cols.index("Dec")
            major_axis_idx = cols.index("MajorAxis")
            minor_axis_idx = cols.index("MinorAxis")
            orientation_idx = cols.index("Orientation")
            I_idx = cols.index("I")
            continue

        # Skip lines starting with "#", they are comments
        if line[0] == "#": continue


        #if i > 2000 : break

        s = line.split(",")

        # Skip lines that contain no commas, they are probably just whitespace
        if len(s) == 1: continue


        source_type = s[type_idx].strip()

        # Skip lines that contain no Type, they are patch definitions
        if len(source_type) == 0: continue

        ra = quantity(s[ra_idx]).get_value("rad")
        dec = quantity(s[dec_idx]).get_value("rad")
        Stokes_I = s[I_idx]

        print(ra,dec)

        min_ra = min(min_ra, ra)
        max_ra = max(max_ra, ra)
        min_dec = min(min_dec, dec)
        max_dec = max(max_dec, dec)

        source = {"type": source_type, "ra": quantity(ra, "rad"), "dec": quantity(dec, "rad"), "I": float(Stokes_I)}

        if source_type == "GAUSSIAN":
            major_axis = s[major_axis_idx]
            minor_axis = s[minor_axis_idx]
            orientation = s[orientation_idx]
            source["major"] = float(major_axis)
            source["minor"] = float(minor_axis)
            source["orientation"] = float(orientation)
            print(major_axis, minor_axis, orientation)


        source_types.add(source_type)

        sources.append(source)


datadir = '/var/scratch/bvdtol/tests/data'
template = os.path.join(datadir,"template.fits")

unit = "rad"
img = casacore.images.image(template)
img.saveas("model.img")
img = casacore.images.image("model.img")

print img.shape()

d = img.getdata()
d[:] = 0
img.putdata(d)
img.tofits("wsclean-Q-model.fits")
img.tofits("wsclean-U-model.fits")
img.tofits("wsclean-V-model.fits")


subgridsize = 160

taper = idgwindow(subgridsize)
taper2 = taper[:, numpy.newaxis] * taper[numpy.newaxis, :]

x,y = numpy.meshgrid(numpy.arange(0.5, subgridsize, 1)/subgridsize-.5, numpy.arange(0.5, subgridsize, 1)/subgridsize-.5)

u = x/img.coordinates().get_increment()[2][0]
v = y/img.coordinates().get_increment()[2][0]

for source in sources:
    pos = img.toworld((0,0,0,0))
    pos[2] = source["dec"].get_value(unit)
    pos[3] = source["ra"].get_value(unit)
    pos = img.topixel(pos)
    rpos = numpy.round(pos)

    subgrid = taper2 * numpy.exp(2*numpy.pi * 1j * (y*(pos[2]-rpos[2]) + x*(pos[3]-rpos[3])))

    if source["type"] == "GAUSSIAN":
        print "Gaussian"
        subgrid = subgrid*GaussianCoherence(source["major"], source["minor"], source["orientation"], u, v)

    print rpos, subgridsize
    subgrid = numpy.fft.fftshift(numpy.fft.fft2(numpy.fft.fftshift(subgrid)))/(subgridsize*subgridsize)
    x1,y1 = numpy.meshgrid(numpy.linspace(-0.5, 0.5, subgridsize, endpoint=False), numpy.linspace(-0.5, 0.5, subgridsize, endpoint=False))
    subgrid *= numpy.exp(-numpy.pi*1j*(x1+y1))

    d[0,0,rpos[2]-subgridsize/2:rpos[2]+subgridsize/2, rpos[3]-subgridsize/2:rpos[3]+subgridsize/2] += source["I"]*subgrid.real


########## Normalization by pointsource in the center
#d1 = d.copy()
#d1[:] = 0
#s = img.shape()
#print s
#subgrid = numpy.abs(numpy.fft.fftshift(numpy.fft.fft2(numpy.fft.fftshift(taper2))))/(subgridsize*subgridsize)
#d1[0,0,s[2]/2-subgridsize/2:s[2]/2+subgridsize/2, s[3]/2-subgridsize/2:s[3]/2+subgridsize/2] = subgrid
#normalization = abs(numpy.fft.fft2(numpy.fft.ifftshift(d1[0,0,:,:])))
#threshold = normalization[0,0]*1e-4

#normalization[normalization<threshold] = threshold

#d[0,0,:,:] = numpy.real(numpy.fft.fftshift(numpy.fft.ifft2(numpy.fft.fft2(numpy.fft.ifftshift(d[0,0,:,:]))/normalization)))

img.putdata(d)
img.tofits("wsclean-I-model.fits")
